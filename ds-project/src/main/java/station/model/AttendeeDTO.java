package station.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class AttendeeDTO {
    private Integer user_id;
    private Integer event_id;
}
