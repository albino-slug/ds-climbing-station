package station.controller;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import station.service.StatsService;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringBufferInputStream;
import java.nio.file.Files;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class AdminViewStatsController {

    @Autowired
    private StatsService statsService;

    @RequestMapping(value = "/user_age_stats", method = RequestMethod.GET)
//    public @ResponseBody Map<String, String> getUserAgeStats() throws IOException {
    public ResponseEntity getUserAgeStats() throws IOException, ClassNotFoundException {
        if (statsService.createUserAgePieChart()){

            return new ResponseEntity<>(HttpStatus.OK);

//            Class cls = Class.forName("AdminViewStatsController");
//            ClassLoader cLoader = cls.getClassLoader();
//            ResponseEntity<byte[]> responseEntity;
//            final HttpHeaders headers = new HttpHeaders();
//            final InputStream in = cLoader.getResourceAsStream("BarChart.jpeg");
//            byte[] media = IOUtils.toByteArray(in);
//            headers.setCacheControl(CacheControl.noCache().getHeaderValue());
//            responseEntity = new ResponseEntity<>(media, headers, HttpStatus.OK);
//            return responseEntity;
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
