package station.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import station.model.Course;
import station.model.Event;
import station.service.CourseService;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class AdminCrudCoursesController {
    @Autowired
    private CourseService courseService;

    @RequestMapping(value = "/admin_add_course", method = RequestMethod.POST)
    public ResponseEntity addNewCourse(@RequestBody Course course) {
        Course savedCourse = courseService.save(course);
        if (savedCourse != null){
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/admin_delete_course", method = RequestMethod.DELETE)
    public ResponseEntity deleteCourse(@RequestParam("id") Integer id){
        if (courseService.deleteById(id)){
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/admin_list_courses", method = RequestMethod.GET)
    public List<Course> listAllCourses(Model model){
        return courseService.findAll();
    }

    @RequestMapping(value = "/admin_update_course", method = RequestMethod.POST)
    public ResponseEntity updateCourse(@RequestBody Course course) {
        Optional foundCourse = courseService.findById(course.getId());
        if (foundCourse.isPresent()){
            Course savedCourse = courseService.save(course);
            if (savedCourse != null){
                return new ResponseEntity<>(HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/admin_find_course_by_id", params = {"id"}, method = RequestMethod.GET)
    public Course findCourse(@RequestParam(value = "id") int id) {
        Optional foundCourse = courseService.findById(id);
        if (foundCourse.isPresent()){
            return (Course)foundCourse.get();
        }
        return new Course();
    }
}
