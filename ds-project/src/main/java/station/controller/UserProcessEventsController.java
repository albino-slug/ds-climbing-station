package station.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import station.model.AttendeeDTO;
import station.model.Event;
import station.model.Mail;
import station.model.User;
import station.service.EventService;
import station.service.MailService;
import station.service.UserService;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class UserProcessEventsController {
    @Autowired
    private EventService eventService;

    @Autowired
    private UserService userService;

    @Autowired
    private MailService mailService;

    @RequestMapping(value = "/user_list_all_events", params = {"id"}, method = RequestMethod.GET)
    public List<Event> listAllEvents(){
        return eventService.findAll();
    }

    @RequestMapping(value = "/user_list_own_events", method = RequestMethod.GET)
    public List<Event> listAllOwnEvents(@RequestParam("id") int id){
        //return userService.findEventsByUserId(Integer.parseInt(httpSession.getAttribute("userId").toString()));

        return userService.findEventsByUserId(id);
    }

    @RequestMapping(value = "/user_attend_event", method = RequestMethod.POST)
    public ResponseEntity attendEvent(@RequestBody AttendeeDTO attendeeDTO){
        //User user = userService.findById(Integer.parseInt(httpSession.getAttribute("userId").toString())).get();
        Optional optionalUser = userService.findById(attendeeDTO.getUser_id());
        if (optionalUser.isPresent()){
            User user = (User)optionalUser.get();
            Boolean addedUser = eventService.addUserById(attendeeDTO.getEvent_id(), user.getId());
            Boolean addedEvent = userService.addEventById(user.getId(), attendeeDTO.getEvent_id());
            if (addedUser && addedEvent){
                System.out.println("attending email");
                mailService.sendSimpleMessage(new Mail("awa.climbing.station@gmail.com", user.getEmail(), "Confirmation for Attending an AWA Event", "You are now on the guest list for the event <<" + eventService.findById(attendeeDTO.getEvent_id()).get().getName() + ">>. You will also get an e-mail reminder a couple of days before the event. Happy climbing! - The AWA Team"));
                return new ResponseEntity<>(HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/user_drop_event", method = RequestMethod.POST)
    public ResponseEntity dropEvent(@RequestBody AttendeeDTO attendeeDTO){
        //User user = userService.findById(Integer.parseInt(httpSession.getAttribute("userId").toString())).get();

        Optional optionalUser = userService.findById(attendeeDTO.getUser_id());
        if (optionalUser.isPresent()){
            User user = (User)optionalUser.get();
            Boolean removedUser = eventService.removeUserById(attendeeDTO.getEvent_id(), attendeeDTO.getUser_id());
            Boolean removedEvent = userService.removeEventById(user.getId(), attendeeDTO.getEvent_id());
            if (removedUser && removedEvent){
                System.out.println("attending email");
                mailService.sendSimpleMessage(new Mail("awa.climbing.station@gmail.com", user.getEmail(), "Dropping Out Confirmation", "You have decided not to attend <<" + eventService.findById(attendeeDTO.getEvent_id()).get().getName() + ">> and are no longer on the waiting list. See you at any of the upcoming events. Happy climbing! - The AWA Team"));
                return new ResponseEntity<>(HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
