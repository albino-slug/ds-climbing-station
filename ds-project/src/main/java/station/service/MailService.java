package station.service;

import station.model.Mail;

public interface MailService {

    void sendSimpleMessage(final Mail mail);

}
