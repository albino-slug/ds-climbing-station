package station.service;

import com.mysql.cj.jdbc.Blob;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import station.model.User;

import java.awt.image.RenderedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;

@Service
public class StatsServiceImpl implements StatsService {

    @Autowired
    private UserService userService;
    private static final String imagePath = "BarChart.jpeg";
    private File image;
    private String dataURI;

    @Override
    public Boolean createUserAgePieChart(){
        DefaultCategoryDataset dataset2 = new DefaultCategoryDataset( );
        List<User> users = userService.findAll();

        // create categories
        int children = 0, teenagers = 0, adults = 0, seniors = 0;

        for (User user : users){
            int age = user.getAge();
            if (age < 12){
               children++;
            }
            else if (age >= 12 && age < 19){
                teenagers++;
            }
            else if (age >= 19 && age < 55){
                adults++;
            }
            else{
                seniors++;
            }
        }

        DefaultPieDataset dataset = new DefaultPieDataset( );
        dataset.setValue("Children="+children, children );
        dataset.setValue("Teenagers="+teenagers, teenagers );
        dataset.setValue("Adults="+adults, adults );
        dataset.setValue("Seniors="+seniors, seniors );

        JFreeChart chart = ChartFactory.createPieChart(
                "User Age Groups",    // chart title
                dataset,                // data
                true,            // include legend
                true,
                false);

        int width = 640;   /* Width of the image */
        int height = 480;  /* Height of the image */
        File pieChart = new File( "PieChart.jpeg" );
        try{
            ChartUtilities.saveChartAsJPEG( pieChart , chart , width , height );
        } catch (IOException e){
            e.printStackTrace();
            return Boolean.FALSE;
        }

        for(User user : users){
            dataset2.setValue(user.getAge() + 0.1, user.getAge().toString(), "age");
        }

        JFreeChart barChart = ChartFactory.createBarChart(
                "USER AGE STATISTICS",
                "Age Category", "Users",
                dataset2, PlotOrientation.VERTICAL,
                true, true, false);

        int width2 = 640;
        int height2 = 480;
        File BarChart = new File(imagePath);
        try{
            ChartUtilities.saveChartAsJPEG( BarChart , barChart , width2 , height2 );

            String contentType = Files.probeContentType(BarChart.toPath());

            byte[] data = Files.readAllBytes(BarChart.toPath());

            String base64str = Base64.getEncoder().encodeToString(data);

            // create "data URI"
            StringBuilder sb = new StringBuilder();
            sb.append("data:");
            sb.append(contentType);
            sb.append(";base64,");
            sb.append(base64str);

            dataURI = sb.toString();
        } catch (IOException e){
            e.printStackTrace();
            return Boolean.FALSE;
        }

        return Boolean.TRUE;
    }
    @Override
    public  Map<String, String> getImage() throws IOException {//todo add path as param
        String encodeImage = Base64.getEncoder().withoutPadding().encodeToString(Files.readAllBytes(image.toPath()));

        Map<String, String> jsonMap = new HashMap<>();

        jsonMap.put("content", encodeImage);

        return jsonMap;
    }

    @Override
    public String getDataURI(){
        return this.dataURI;
    }
}
