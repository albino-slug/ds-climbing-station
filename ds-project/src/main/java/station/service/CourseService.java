package station.service;

import station.model.Course;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface CourseService {

    List<Course> findAll();

    Course save(Course course);

    Boolean deleteById(Integer id);

    Boolean addUserById(Integer courseId, Integer userId);

    Boolean removeUserById(Integer courseId, Integer userId);

    Optional<Course> findById(Integer id);

    List<Course> findByStartDate(Date date);

    Optional<Course> findByName(String name);
}
