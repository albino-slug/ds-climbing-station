package station.service;

import station.model.User;

import java.util.Optional;

public interface AuthenticationService {

    Optional<User> loadByNameAndPassword(String username, String password);

}
